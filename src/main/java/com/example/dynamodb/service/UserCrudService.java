package com.example.dynamodb.service;

import com.example.dynamodb.model.User;

public interface UserCrudService {
    User createUser(User user);

    User readUser(String userId);

    User updateUser(User user);

    void deleteUser(String userId);
}
