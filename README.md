DinamoDB
===
Implementação do projeto [Quick Start Guide: Spring Boot CRUD with DynamoDB](https://medium.com/@nuozhoux/get-started-guide-spring-boot-crud-with-dynamodb-991e6341844c)

## Observação
Altere a configuração da classe **DynamoDBConfig**, inserindo a sua credencial IAM no AWS, com a política de permissão **AmazonDynamoDBFullAccess**

    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("ID da chave de acesso", "Chave de acesso secreta")))

## Software stack
*  Java 8
*  Maven
*  Spring Boot
*  DynamoDB
*  Lombok